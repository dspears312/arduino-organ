/*  Arduino Due Organ Script
 *  written by Daniel Spears
 */

// Set this to the MIDI channel you want to use:
const int midi_channel = 0;


#include <YetAnotherDebouncer.h>
#include "MIDIUSB.h"
#include "PitchToNote.h"

void noteOn(byte channel, byte pitch, byte velocity) {

  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};

  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {

  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};

  MidiUSB.sendMIDI(noteOff);
}

const int KEY_COUNT = 61;
const int DEBOUNCE_TIME = 2;

 int pins[KEY_COUNT] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, A0, A1, A2, A3, A4, A5, A6, A7, A8};
 int notes[KEY_COUNT] = {
  pitchC2, pitchD2b, pitchD2, pitchE2b, pitchE2, pitchF2, pitchG2b, pitchG2, pitchA2b, pitchA2, pitchB2b, pitchB2, 
  pitchC3, pitchD3b, pitchD3, pitchE3b, pitchE3, pitchF3, pitchG3b, pitchG3, pitchA3b, pitchA3, pitchB3b, pitchB3,
  pitchC4, pitchD4b, pitchD4, pitchE4b, pitchE4, pitchF4, pitchG4b, pitchG4, pitchA4b, pitchA4, pitchB4b, pitchB4,
  pitchC5, pitchD5b, pitchD5, pitchE5b, pitchE5, pitchF5, pitchG5b, pitchG5, pitchA5b, pitchA5, pitchB5b, pitchB5,
  pitchC6, pitchD6b, pitchD6, pitchE6b, pitchE6, pitchF6, pitchG6b, pitchG6, pitchA6b, pitchA6, pitchB6b, pitchB6,
  pitchC7 };
int state[KEY_COUNT] = {};

Debouncer<Clock::Millis> debouncers[KEY_COUNT];

void debounce_callback(bool value, int* i) {
//  Serial.print("Button #");
//  Serial.print(*i);
//  Serial.print(" changed to ");
//  Serial.println(value ? "OFF" : "ON");

  if (!value) {
    noteOn(midi_channel, *i, 127);
  } else {
    noteOff(midi_channel, *i, 0);
  }
  
  MidiUSB.flush();
}

void setup() {
  // put your setup code here, to run once:
//  Serial.begin(9600);

  for (int i = 0; i < KEY_COUNT; i++) {
    pinMode(pins[i], INPUT);
    state[i] = 0;

    debouncers[i].begin();
    debouncers[i].set_debounce_stable(DEBOUNCE_TIME); // ms
    debouncers[i].set_callback(debounce_callback, &notes[i]);
  }
}



void loop() {
  for (int i = 0; i < KEY_COUNT; i++) {
    debouncers[i].set_value(digitalRead(pins[i]));
  }

}